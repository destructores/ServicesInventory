﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WCFUsuariosTest
{
    [TestClass]
    public class UsuarioTest
    {
        [TestMethod]
        public void ObtenerUsuarioTestOk()
        {
            UsuariosWS.UsuariosServiceClient proxy = new UsuariosWS.UsuariosServiceClient();
            var usuarios = proxy.ObtenerUsuario(403212);
            Assert.AreEqual(403212, usuarios.Dni);
        }

        [TestMethod]
        public void ObtenerUsuarioTestNoOK()
        {
            UsuariosWS.UsuariosServiceClient proxy = new UsuariosWS.UsuariosServiceClient();
            try
            {
                var usuarios = proxy.ObtenerUsuario(403290);
            }
            catch (Exception e)
            {
                string value = e.Message;
                Assert.AreEqual("No existe usuario", value);
            }

        }

        [TestMethod]
        public void ListarUsuarioTestOk()
        {
            UsuariosWS.UsuariosServiceClient proxy = new UsuariosWS.UsuariosServiceClient();
            var usuarios = proxy.ListarUsuarios();
            Assert.AreEqual(2, usuarios.Length);

        }

        [TestMethod]
        public void ListarUsuarioTestNoOK()
        {
            UsuariosWS.UsuariosServiceClient proxy = new UsuariosWS.UsuariosServiceClient();
            try
            {
                var usuarios = proxy.ListarUsuarios();
            }
            catch (Exception e)
            {
                string value = e.Message;
                Assert.AreEqual("Error interno", value);
            }

        }

    }
}

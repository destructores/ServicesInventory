﻿using ServicesInventory.Dominio;
using ServicesInventory.Errores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicesInventory
{
    
    [ServiceContract]
    public interface ISalasService
    {

        [FaultContract(typeof(SalaException))]
        [OperationContract]
        List<Sala> ObtenerSala(string identificadorZona);

        [FaultContract(typeof(SalaException))]
        [OperationContract]
        List<Sala> ListarSalas();

       
    }
}

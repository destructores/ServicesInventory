﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WCFSalasTest
{
    [TestClass]
    public class SalaTest
    {
        [TestMethod]
        public void ObtenerSalaTestOK()
        {
            SalasWS.SalasServiceClient proxy = new SalasWS.SalasServiceClient();
            var salas = proxy.ObtenerSala("SURCO");
            Assert.AreEqual(3,salas.Length);
        }

        [TestMethod]
        public void ObtenerSalaTestNoOK()
        {
            SalasWS.SalasServiceClient proxy = new SalasWS.SalasServiceClient();
            try
            {
                var salas = proxy.ObtenerSala("SURCO");
                Assert.AreEqual(3, salas.Length);
            }
            catch (Exception e)
            {
                string value = e.Message;
                Assert.AreEqual("No existe sala", value);
            }
        }

        [TestMethod]
        public void ListarSalaTestOk()
        {
            SalasWS.SalasServiceClient proxy = new SalasWS.SalasServiceClient();
            var salas = proxy.ListarSalas();
            Assert.AreEqual(4, salas.Length);

        }

        [TestMethod]
        public void ListarSalaTestNoOK()
        {
            SalasWS.SalasServiceClient proxy = new SalasWS.SalasServiceClient();
            try
            {
                var salas = proxy.ListarSalas();
            }
            catch (Exception e)
            {
                string value = e.Message;
                Assert.AreEqual("Error interno", value);
            }

        }

    }
}
﻿using ServicesInventory.Dominio;
using ServicesInventory.Errores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicesInventory
{
    
    [ServiceContract]
    public interface IUsuariosService
    {
        [FaultContract(typeof(UsuarioException))]
        [OperationContract]
        Usuario ObtenerUsuario(int dni);

        [FaultContract(typeof(UsuarioException))]
        [OperationContract]
        List<Usuario> ListarUsuarios();


    }
}

﻿using ServicesInventory.Dominio;
using ServicesInventory.Errores;
using ServicesInventory.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicesInventory
{
    
    public class SalasService : ISalasService
    {
        private SalaDAO saladao = new SalaDAO();

        public List<Sala> ObtenerSala(string identificadorZona)
        {
            List<Sala> salas = null;

            try
            {
                salas = saladao.ObtenerSalaPorZona(identificadorZona);
            }
            catch
            {
                throw new FaultException<SalaException>(new SalaException()
                {
                    Codigo = "0002",
                    Descripcion = "Error interno"
                },
                   new FaultReason("Error interno"));
                throw new NotImplementedException();

            }

            if (salas.Count > 0)
            {
                return salas;
            }
            else
            {
                throw new FaultException<SalaException>(new SalaException()
                {
                    Codigo = "0001",
                    Descripcion = "No existe sala"
                },
                   new FaultReason("No existe sala"));
                throw new NotImplementedException();
            }

        }


        public List<Sala> ListarSalas()
        {
            List<Sala> listasala = null;


            try
            {
                listasala = saladao.Listar();
                return listasala;
            }
            catch
            {
                throw new FaultException<SalaException>(new SalaException()
                {
                    Codigo = "0002",
                    Descripcion = "Error interno"
                },
                   new FaultReason("Error interno"));
                throw new NotImplementedException();

            }

        }

       
    }
}

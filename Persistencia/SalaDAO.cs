﻿using ServicesInventory.Dominio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServicesInventory.Persistencia
{
    public class SalaDAO
    {
        //private string cadenaConexion = "Data Source=(local)\\SQLEXPRESS;Initial Catalog=ProyectoSALAS01;Integrated Security=SSPI;";
        private string cadenaConexion = "Data Source=(local)\\SQLEXPRESS;Initial Catalog=DBSalas;Integrated Security=SSPI;";

        
        public List<Sala> Listar()
        {

            List<Sala> salasEncontradas = new List<Sala>();
            Sala salaEncontrada = null;
            string sql = "SELECT * from t_sala";

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            salaEncontrada = new Sala()
                            {
                                Id = (int)resultado["nu_id"],
                                Nombre = (string)resultado["tx_nombre"],
                                Direccion = (string)resultado["tx_direccion"],
                                Referencia = (string)resultado["tx_referencia"],
                                Capacidad = (int)resultado["nu_capacidad"],
                                Contacto = (string)resultado["tx_contacto"],
                                Correo = (string)resultado["tx_correoelectronico"],
                                Telefono = (int)resultado["nu_telefono"],
                                Distrito = (string)resultado["tx_distrito"],
                                Zona = (string)resultado["tx_zona"],

                            };
                            salasEncontradas.Add(salaEncontrada);
                        }
                    }
                }
            }
            return salasEncontradas;

        }


        public List<Sala> ObtenerSalaPorZona(string zona)
        {
            List<Sala> salasEncontradas = new List<Sala>();
            Sala salaEncontrada = null;
            string sql = "SELECT * from t_sala WHERE tx_zona=@tx_zona";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@tx_zona", zona));
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            salaEncontrada = new Sala()
                            {
                                Id = (int)resultado["nu_id"],
                                Nombre = (string)resultado["tx_nombre"],
                                Direccion = (string)resultado["tx_direccion"],
                                Referencia = (string)resultado["tx_referencia"],
                                Capacidad = (int)resultado["nu_capacidad"],
                                Contacto = (string)resultado["tx_contacto"],
                                Correo = (string)resultado["tx_correoelectronico"],
                                Telefono = (int)resultado["nu_telefono"],
                                Distrito = (string)resultado["tx_distrito"],
                                Zona = (string)resultado["tx_zona"],
                            };
                            salasEncontradas.Add(salaEncontrada);
                        }
                    }

                }
            }
            return salasEncontradas;
        }

    }
}